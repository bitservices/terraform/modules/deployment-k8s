<!---------------------------------------------------------------------------->

# argocd/application/helm

#### Provisions [Argo CD] applications that are based on [Helm] charts

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/deployment/k8s//argocd/application/helm`**

-------------------------------------------------------------------------------

### Example Usage

```
resource "kubernetes_namespace" "my_namespace" {
  metadata {
    name = "foobar"
  }
}

module "my_argocd_application" {
  source           = "gitlab.com/bitservices/deployment/k8s//argocd/application/helm"
  name             = "foobar"
  repo_url         = "git@gitlab.com:bitservices/foobar.git"
  namespace        = kubernetes_namespace.my_namespace.metadata.0.name
  argocd_namespace = "argocd"
}
```

<!---------------------------------------------------------------------------->

[Helm]:    https://helm.sh/
[Argo CD]: https://argoproj.github.io/cd/

<!---------------------------------------------------------------------------->
