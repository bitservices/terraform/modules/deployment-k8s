###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The name for the ArgoCD application."
}

variable "namespace" {
  type        = string
  description = "Namespace that this application should be deployed in to."
}

###############################################################################

variable "argocd_namespace" {
  type        = string
  description = "The namespace that ArgoCD is deployed into."
}

###############################################################################

variable "repo_url" {
  type        = string
  description = "The repository URL for this application."
}

###############################################################################
# Optional Variables
###############################################################################

variable "kind" {
  type        = string
  default     = "Application"
  description = "Identifier kind for ArgoCD applications. This should never be overridden."
}

variable "labels" {
  type        = map(string)
  default     = {}
  description = "Additional labels to be assigned to the ArgoCD application."
}

variable "values" {
  type = list(object({
    name  = string
    value = string
  }))
  default     = []
  description = "A list of Helm override values."
}

variable "cluster" {
  type        = string
  default     = "https://kubernetes.default.svc"
  description = "Target cluster for this application deployment."
}

variable "project" {
  type        = string
  default     = "default"
  description = "The project this application belongs to."
}

variable "release" {
  type        = string
  default     = null
  description = "The Helm release name. Defaults to the application name if not specified."
}

variable "name_label" {
  type        = string
  default     = "app.kubernetes.io/name"
  description = "The label key for the label holding the applications name."
}

variable "annotations" {
  type        = map(string)
  default     = {}
  description = "Annotations to be assigned to the ArgoCD application."
}

variable "api_version" {
  type        = string
  default     = "argoproj.io/v1alpha1"
  description = "The ArgoCD custom resource definition API version."
}

variable "value_files" {
  type        = list(string)
  default     = []
  description = "List of Helm values files relative to 'repo_path'."
}

variable "cascade_delete" {
  type        = bool
  default     = true
  description = "Cascade delete all application objects."
}

###############################################################################

variable "repo_ref" {
  type        = string
  default     = "latest"
  description = "The reference within the repository to deploy. For Git, this could be a branch or tag."
}

variable "repo_path" {
  type        = string
  default     = null
  description = "The path within the repository that contains the Helm chart."
}

###############################################################################

variable "sync_options" {
  type = list(string)
  default = [
    "ApplyOutOfSyncOnly=true",
    "CreateNamespace=false"
  ]
  description = "Sync options which modifies sync behavior."
}

variable "sync_auto_enabled" {
  type        = bool
  default     = false
  description = "Should ArgoCD automatically sync configuration changes?"
}

variable "sync_auto_heal" {
  type        = bool
  default     = true
  description = "Specifies if partial app sync should be executed when resources are changed only in target Kubernetes cluster and no git change detected."
}

variable "sync_auto_empty" {
  type        = bool
  default     = false
  description = "Allows deleting all application resources during automatic syncing."
}

variable "sync_auto_prune" {
  type        = bool
  default     = true
  description = "Specifies if resources should be pruned during auto-syncing."
}

variable "sync_retry_limit" {
  type        = number
  default     = 5
  description = "Number of failed sync attempt retries or '0' for unlimited."
}

variable "sync_retry_backoff_duration" {
  type        = string
  default     = "5s"
  description = "The amount to back off. Default unit is seconds if no unit ('s', 'm', or 'h') is specified."
}

variable "sync_retry_backoff_factor" {
  type        = number
  default     = 2
  description = "A factor to multiply the base duration after each failed retry."
}

variable "sync_retry_backoff_max" {
  type        = string
  default     = "10m"
  description = "The maximum amount of time allowed for the backoff strategy. Default unit is seconds if no unit ('s', 'm', or 'h') is specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  release    = coalesce(var.release, var.name)
  repo_path  = coalesce(var.repo_path, format("%s/chart/%s", var.name, var.name))
  finalizers = var.cascade_delete ? tolist(["resources-finalizer.argocd.argoproj.io"]) : []

  sync_auto_map = {
    "automated" = {
      "prune"      = var.sync_auto_prune
      "selfHeal"   = var.sync_auto_heal
      "allowEmpty" = var.sync_auto_empty
    }
  }
}

###############################################################################
# Resources
###############################################################################

resource "kubernetes_manifest" "object" {
  manifest = {
    "apiVersion" = var.api_version
    "kind"       = var.kind

    "metadata" = merge({
      "name"      = var.name
      "labels"    = merge({ (var.name_label) = var.name }, var.labels)
      "namespace" = var.argocd_namespace
      },
      {
        for k, v in {
          "annotations" = var.annotations
          "finalizers"  = local.finalizers
        } :
        k => v
        if length(v) >= 1
    })

    "spec" = {
      "project" = var.project

      "source" = {
        "repoURL"        = var.repo_url
        "targetRevision" = var.repo_ref
        "path"           = local.repo_path

        helm = merge({
          "releaseName" = local.release
          },
          {
            for k, v in {
              "parameters" = var.values
              "valueFiles" = var.value_files
            } :
            k => v
            if length(v) >= 1
        })
      }

      "destination" = {
        "namespace" = var.namespace
        "server"    = var.cluster
      }

      syncPolicy = merge(var.sync_auto_enabled ? local.sync_auto_map : {}, {
        "syncOptions" = var.sync_options

        "retry" = {
          "limit" = var.sync_retry_limit

          "backoff" = {
            "duration"    = var.sync_retry_backoff_duration
            "factor"      = var.sync_retry_backoff_factor
            "maxDuration" = var.sync_retry_backoff_max
          }
        }
      })
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "namespace" {
  value = var.namespace
}

###############################################################################

output "repo_url" {
  value = var.repo_url
}

###############################################################################

output "values" {
  value = var.values
}

output "cluster" {
  value = var.cluster
}

output "project" {
  value = var.project
}

output "release" {
  value = local.release
}

output "name_label" {
  value = var.name_label
}

output "annotations" {
  value = var.annotations
}

output "value_files" {
  value = var.value_files
}

output "cascade_delete" {
  value = var.cascade_delete
}

###############################################################################

output "repo_ref" {
  value = var.repo_ref
}

output "repo_path" {
  value = local.repo_path
}

###############################################################################

output "sync_options" {
  value = var.sync_options
}

output "sync_auto_enabled" {
  value = var.sync_auto_enabled
}

output "sync_auto_heal" {
  value = var.sync_auto_heal
}

output "sync_auto_empty" {
  value = var.sync_auto_empty
}

output "sync_auto_prune" {
  value = var.sync_auto_prune
}

output "sync_retry_limit" {
  value = var.sync_retry_limit
}

output "sync_retry_backoff_duration" {
  value = var.sync_retry_backoff_duration
}

output "sync_retry_backoff_factor" {
  value = var.sync_retry_backoff_factor
}

output "sync_retry_backoff_max" {
  value = var.sync_retry_backoff_max
}

###############################################################################

output "name" {
  value = kubernetes_manifest.object.manifest.metadata.name
}

output "kind" {
  value = kubernetes_manifest.object.manifest.kind
}

output "labels" {
  value = kubernetes_manifest.object.manifest.metadata.labels
}

output "api_version" {
  value = kubernetes_manifest.object.manifest.apiVersion
}

output "argocd_namespace" {
  value = kubernetes_manifest.object.manifest.metadata.namespace
}

###############################################################################
