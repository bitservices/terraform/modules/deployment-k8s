<!---------------------------------------------------------------------------->

# argocd/project

#### Manages [Argo CD] projects to group applications and manage permissions

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/deployment/k8s//argocd/project`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_argocd_project" {
  source           = "gitlab.com/bitservices/deployment/k8s//argocd/project"
  name             = "foobar"
  argocd_namespace = "argocd"
}
```

<!---------------------------------------------------------------------------->

[Argo CD]: https://argoproj.github.io/cd/

<!---------------------------------------------------------------------------->
