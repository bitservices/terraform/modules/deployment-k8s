###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The name for the ArgoCD project."
}

###############################################################################

variable "argocd_namespace" {
  type        = string
  description = "The namespace that ArgoCD is deployed into."
}

###############################################################################
# Optional Variables
###############################################################################

variable "kind" {
  type        = string
  default     = "AppProject"
  description = "Identifier kind for ArgoCD projects. This should never be overridden."
}

variable "repos" {
  type        = list(string)
  default     = ["*"]
  description = "List of repositories this project can deploy from."

  validation {
    condition     = length(compact(var.repos)) >= 1
    error_message = "The project must be able to deploy from at least one repository."
  }
}

variable "labels" {
  type        = map(string)
  default     = {}
  description = "Additional labels to be assigned to the ArgoCD project."
}

variable "cluster" {
  type        = string
  default     = "https://kubernetes.default.svc"
  description = "Cluster this project can deploy into."
}

variable "name_label" {
  type        = string
  default     = "app.kubernetes.io/name"
  description = "The label key for the label holding the projects name."
}

variable "namespaces" {
  type        = list(string)
  default     = ["*"]
  description = "Namespaces that this project can deploy in to. Use '*' for any/all."

  validation {
    condition     = length(var.namespaces) > 0
    error_message = "At least one namespace that this project can deploy to must be specified!"
  }

  validation {
    condition     = (!contains(var.namespaces, "*")) || length(var.namespaces) == 1
    error_message = "If specifying all namespaces with '*' then only one namespace destination should be specified!"
  }
}

variable "api_version" {
  type        = string
  default     = "argoproj.io/v1alpha1"
  description = "The ArgoCD custom resource definition API version."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Description for the project."
}

variable "orphan_warn" {
  type        = bool
  default     = false
  description = "Warn on orphaned namespace resources."
}

###############################################################################

variable "roles" {
  default     = []
  description = "A list of ArgoCD RBAC roles to apply to this project."

  type = list(object({
    name        = string
    groups      = set(string)
    policies    = list(string)
    description = string

    tokens = list(object({
      id  = string
      exp = number
      iat = number
    }))
  }))
}

###############################################################################

variable "cluster_resource_blacklist" {
  default     = []
  description = "A list of cluster-wide resource types that are blocked from being managed by this project."

  type = list(object({
    group = string
    kind  = string
  }))
}

variable "cluster_resource_whitelist" {
  default     = []
  description = "A list of cluster-wide resource types that are allowed to be managed by this project."

  type = list(object({
    group = string
    kind  = string
  }))
}

variable "namespace_resource_blacklist" {
  default     = []
  description = "A list of namespace resource types that are blocked from being managed by this project."

  type = list(object({
    group = string
    kind  = string
  }))
}

variable "namespace_resource_whitelist" {
  description = "A list of namespace resource types that are allowed to be managed by this project."

  default = [
    {
      "group" = "*"
      "kind"  = "*"
    }
  ]

  type = list(object({
    group = string
    kind  = string
  }))
}

###############################################################################
# Resources
###############################################################################

resource "kubernetes_manifest" "object" {
  manifest = {
    "apiVersion" = var.api_version
    "kind"       = var.kind

    "metadata" = {
      "name"      = var.name
      "labels"    = merge({ (var.name_label) = var.name }, var.labels)
      "namespace" = var.argocd_namespace

      "finalizers" = [
        "resources-finalizer.argocd.argoproj.io"
      ]
    }

    "spec" = {
      "sourceRepos"                = var.repos
      "description"                = var.description
      "clusterResourceBlacklist"   = var.cluster_resource_blacklist
      "clusterResourceWhitelist"   = var.cluster_resource_whitelist
      "namespaceResourceBlacklist" = var.namespace_resource_blacklist
      "namespaceResourceWhitelist" = var.namespace_resource_whitelist

      "destinations" = [
        for namespace in var.namespaces : {
          "namespace" = namespace
          "server"    = var.cluster
        }
      ]

      "orphanedResources" = {
        "warn" = var.orphan_warn
      }

      "roles" = [
        for role in var.roles :
        {
          for k, v in role :
          k == "tokens" ? "jwtTokens" : k => v
        }
      ]
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "repos" {
  value = var.repos
}

output "cluster" {
  value = var.cluster
}

output "name_label" {
  value = var.name_label
}

output "namespaces" {
  value = var.namespaces
}

output "description" {
  value = var.description
}

output "orphan_warn" {
  value = var.orphan_warn
}

###############################################################################

output "roles" {
  value = var.roles
}

###############################################################################

output "cluster_resource_blacklist" {
  value = var.cluster_resource_blacklist
}

output "cluster_resource_whitelist" {
  value = var.cluster_resource_whitelist
}

output "namespace_resource_blacklist" {
  value = var.namespace_resource_blacklist
}

output "namespace_resource_whitelist" {
  value = var.namespace_resource_whitelist
}

###############################################################################

output "name" {
  value = kubernetes_manifest.object.manifest.metadata.name
}

output "kind" {
  value = kubernetes_manifest.object.manifest.kind
}

output "labels" {
  value = kubernetes_manifest.object.manifest.metadata.labels
}

output "api_version" {
  value = kubernetes_manifest.object.manifest.apiVersion
}

output "argocd_namespace" {
  value = kubernetes_manifest.object.manifest.metadata.namespace
}

###############################################################################
