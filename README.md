<!---------------------------------------------------------------------------->

# deployment (k8s)

<!---------------------------------------------------------------------------->

## Description

Manages [Kubernetes] resources that are responsible for the deployment of
applications and services.

<!---------------------------------------------------------------------------->

## Modules

* [argocd/application/helm](argocd/application/helm/README.md) - Provisions [Argo CD] applications that are based on [Helm] charts.
* [argocd/project](argocd/project/README.md) - Manages [Argo CD] projects to group applications and manage permissions.

<!---------------------------------------------------------------------------->

[Helm]:       https://helm.sh/
[Argo CD]:    https://argoproj.github.io/cd/
[Kubernetes]: https://kubernetes.io/

<!---------------------------------------------------------------------------->
