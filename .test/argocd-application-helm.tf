###############################################################################
# Modules
###############################################################################

module "argocd_application_helm" {
  source           = "../argocd/application/helm"
  name             = local.name
  project          = module.argocd_project.name
  repo_url         = "git@gitlab.com:bitservices/foobar.git"
  namespace        = local.namespace
  argocd_namespace = local.argocd_namespace
}

###############################################################################
